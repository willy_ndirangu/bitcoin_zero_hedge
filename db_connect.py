import MySQLdb
from config import *


class DatabaseOPs(object):
    """
    This class exposes functions related to the database
    """

    def __init__(self):
        """ Initialize variables that are needed """
        self.host = DB_HOST
        self.db = DB
        self.passwd = DB_PASSWD
        self.user = DB_USER

    def connect_to_mysql(self):

        """
        Connection to the Mysql database

        Args:
            host (str): the database host
            user (str): database user
            passwd (str): the users password
            db (str): the database to connect to

       Returns:
                the database connection


            """
        try:
            conn = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd, db=self.db)
            return conn
        except:
            print("Database Connection error")

        def query_db(self, query):
            """
        Query the database
        
        Args:
            query (str): the query associated with a certain request
           
        
       Returns:
            the results from the database
        
        
        """
