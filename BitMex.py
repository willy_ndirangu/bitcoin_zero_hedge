from bravado.client import SwaggerClient
from bravado.requests_client import RequestsClient
from api_con.BitMEXAPIKeyAuthenticator import APIKeyAuthenticator
import json
import pprint
from config import *


class BitMex(object):
    """
    This class contains methods to connect and perform actions on the Bitmex platform using the rest api
    """

    def __init__(self):
        """
        initializing variables that we will require in the class
        
        """
        self.bitMEXAuthenticated = self.auth_details()

    def auth_details(self):
        """
        This functions ensures that authentication is done to enable access to bitmex API endpoints
        
    
        Returns:
            Authenticated client.

        
        """

        # Set the rest request client for sending orders. This has to be authenticated
        request_client = RequestsClient()
        request_client.authenticator = APIKeyAuthenticator(HOST, BITMEX_API_KEY, BITMEX_API_SECRET)

        bitMEXAuthenticated = SwaggerClient.from_url(SPEC_URI, config=self.config_details(), http_client=request_client)

        return bitMEXAuthenticated

    def config_details(self):
        """
        Provides the configuration details for the rest api endpoint to enable quering and posting.

        Returns:
            dict. configuration specifications.

        """
        # See full config options at http://bravado.readthedocs.io/en/latest/configuration.html
        config_swag = {
            # Don't use models (Python classes) instead of dicts for #/definitions/{models}
            'use_models': False,
            # This library has some issues with nullable fields
            'validate_responses': False,
            # Returns response in 2-tuple of (body, response); if False, will only return body
            'also_return_response': True,}

        return config_swag



        # futures transactions

    def buy_futures(self, symbol="XBTH18", orderQty=1, price=0):
        """
        Buy a specified amount of futures contract at market value
        
        Args:
            symbol (str): The futures symbol
            orderQty (int): The number of contracts to be bought defaults to one
            price (float): The price to use when buying the contracts
                
        Returns:
            resp: a list with the details of the bought contracts if successful, None otherwise.
        
        """
        resp, http_response = self.bitMEXAuthenticated.Order.Order_new(symbol=symbol, orderQty=orderQty, side='Buy',
                                                                       price=price,
                                                                       ordType="Limit").result()

        return resp

    def sell_futures(self, symbol="XBTH18", orderQty=1, price=0):
        """
        Sell a specified amount of futures contract at market value
        
        Args:
            symbol (str): The futures symbol
            orderQty (int): The number of contracts to be sold defaults to one
            price (float): The price to use when selling the contracts
                
        Returns:
            resp: a list with the details of the sold contracts if successful, None otherwise.

        """

        resp, http_response = self.bitMEXAuthenticated.Order.Order_new(symbol=symbol, orderQty=orderQty, side='Sell',
                                                                       price=price, ordType="Limit").result()

        return resp

    # spot transactions

    def buy_spot(self, symbol="XBTUSD", orderQty=1, price=0):
        """
        Buy a specified amount of spot contract at market value
        
        Args:
            symbol (str): The spot symbol
            orderQty (int): The number of contracts to be bought defaults to one
            price (float): The price to use when buying the contracts
                
        Returns:
            resp: a list with the details of the bought contracts if successful, None otherwise.
        """
        resp, http_response = self.bitMEXAuthenticated.Order.Order_new(symbol=symbol, orderQty=orderQty, side='Buy',
                                                                       price=price, ordType="Limit").result()
        return resp

    def sell_spot(self, symbol="XBTUSD", orderQty=1, price=0):
        """
        Sell a specified amount of spot contract at market value
        
        Args:
            symbol (str): The spot symbol
            orderQty (int): The number of contracts to be sold defaults to one
            price (float): The price to use when buying the contracts
                
        Returns:
            resp: a list with the details of the sold contracts if successful, None otherwise.

        """

        resp, http_response = self.bitMEXAuthenticated.Order.Order_new(symbol=symbol, orderQty=orderQty, side='Sell',
                                                                       price=price, ordType="Limit").result()
        return resp

    # Check transactions

    def get_positions(self, symbol):
        """
        Get the position given the symbol
        
        Args:
            symbol (str): The  symbol of the positions to get
            
                
        Returns:
            resp: a list with the details of the open positions  if successful, None otherwise.

        """
        resp, http_response = self.bitMEXAuthenticated.Position.Position_get(
            filter=json.dumps({"symbol": symbol, "isOpen": True})).result()

        # check if there is postion 
        if not resp:
            return None

        return resp

    def get_open_orders(self, symbol):

        """
        Get the open  orders
        
        Args:
            symbol (str): The  symbol of the orders to get
                        
                
        Returns:
            resp: a list with the details of the orders that are not filled  if successful, None otherwise.

        """
        resp, http_response = self.bitMEXAuthenticated.Order.Order_getOrders(symbol=symbol, reverse=True,
                                                                             filter=json.dumps(
                                                                                 {"ordStatus": "New"})).result()

        if not resp:
            return None
        return resp

    # change  order to market

    def change_order_to_market(self, orderID, price):
        """
        Change to market order
        
        Args:
            orderID (str): The  unique string that is used to obtain a certain order to amend
            price (float): The price to use when amending  the order
            
                
        Returns:
            resp: a list with the details of the amended order  if successful, None otherwise.
        """
        resp, http_response = self.bitMEXAuthenticated.Order.Order_amend(orderID=orderID, price=price).result()

        if not resp:
            return None
        return resp

    # get an instrument details

    def get_instrument_details(self, symbol):
        """
        Get the details associated with a certain instrument

        Args:
            symbol (str): The  symbol of the instrument to get
                        
                
        Returns:
            resp: a list with the details of the instrument if successful, None otherwise.

        """

        resp, http_response = self.bitMEXAuthenticated.Instrument.Instrument_get(symbol=symbol, reverse=True).result()
        if not resp:
            return None

        return resp

    # get nargin leverage used
    def margin_leverage(self):
        """
        Get the leveraged margin associated with a certain used 
        
        
        Returns:
            res (float): margin leverage used
        
        """
        res, http = self.bitMEXAuthenticated.User.User_getMargin().result()
        return res['marginLeverage']

    # get active instruments

    def get_active_instruments(self):
        """
        Gets the instruments being traded

        Returns:
            resp : a list of active instruments
        """
        resp, http_response = self.bitMEXAuthenticated.Instrument.Instrument_getActive().result()
        if not resp:
            return None

        return resp

    def check_for_unfilled_orders(self, orders):

        """
        Checks  for unfilled orders
        Args:
                orders  (list): A list of orders


            Returns:
                unfillede: a list with the details of unfilled orders.
        """
        unfilled = [order for order in orders if order["ordStatus"] == "New" or order["ordStatus"] == "PartiallyFilled"]

        if not unfilled:
            return None

        return unfilled

    def get_wallet_history(self):

        """
        This function get's the wallet history based on the user

        Returns:
                resp : a list of wallet history profits and loses
        """
        resp, http = self.bitMEXAuthenticated.User.User_getWalletHistory().result()

        if not resp:
            return []
        return resp
