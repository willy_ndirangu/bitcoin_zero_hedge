from db_connect import DatabaseOPs
from BitMex import BitMex
from api_con.bitmex_websocket import BitMEXWebsocket
import logging
from time import sleep
import time
import datetime
from pykalman import KalmanFilter
import statsmodels
import statsmodels.api as sm
from  config import *


bitmex=BitMex()


future = BitMEXWebsocket(endpoint=BITMEX_TEST_WEB_SOCKET_URL, symbol="XBTH18",api_key=BITMEX_API_KEY, api_secret=BITMEX_API_SECRET)

spot = BitMEXWebsocket(endpoint=BITMEX_TEST_WEB_SOCKET_URL, symbol="XBTUSD",
                         api_key=BITMEX_API_KEY, api_secret=BITMEX_API_SECRET)