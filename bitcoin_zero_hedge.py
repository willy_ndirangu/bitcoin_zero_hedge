# modules to work with the bitmex api
from bravado.client import SwaggerClient
from bravado.requests_client import RequestsClient
from api_con.BitMEXAPIKeyAuthenticator import APIKeyAuthenticator
import json
import pprint
from config import *
import pandas as pd
import numpy as np
import peakutils.peak
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score,mean_squared_error
#import matplotlib.pyplot as plt
from scipy import stats
from api_con.bitmex_websocket import BitMEXWebsocket
import logging
from time import sleep
import time
import datetime
from pykalman import KalmanFilter
import statsmodels
import statsmodels.api as sm
import MySQLdb



# See full config options at http://bravado.readthedocs.io/en/latest/configuration.html
config_swag = {
  # Don't use models (Python classes) instead of dicts for #/definitions/{models}
  'use_models': False,
  # This library has some issues with nullable fields
  'validate_responses': False,
  # Returns response in 2-tuple of (body, response); if False, will only return body
  'also_return_response': True,
    
    
    
}

# connecting to the bitmex rest api
bitMEX = SwaggerClient.from_url(
  SPEC_URI,
  config=config_swag)

pp = pprint.PrettyPrinter(indent=2)


res, http_response = bitMEX.Trade.Trade_get(symbol=".BXBT30M",count=500,reverse=True).result()

#create a dataframe to for the index used by futures
bt30=pd.DataFrame(res)

#get the data associated with the bitcoin index spot prices at exactly 12.00 utc since the futures data provided is also from the same time
resp, http_response = bitMEX.Trade.Trade_get(symbol=".BXBT",count=500,reverse=True,filter=json.dumps({'timestamp.time':"12:00:00.000" })).result()

#create a dataframe to for the index used by spot
bt1=pd.DataFrame(resp)

#set the dataframes to use timestamp for the indexes inorder to merge
bt30.set_index("timestamp")
bt1.set_index("timestamp")

#merge the two dataframes
merged=pd.merge(bt30,bt1, how='inner', left_index=True, right_index=True)

# select few columns from the merged dataframe 
truncated_merged_df=merged[["symbol_x","timestamp_x","price_x","symbol_y","timestamp_y","price_y"]].copy()

#get the percentage change for the spot prices

truncated_merged_df["percentage_difference_bt1"]=0
truncated_merged_df.loc[:,"percentage_difference_bt1"]=(truncated_merged_df["price_y"].diff(periods=1)/truncated_merged_df["price_y"])*100

# backfill nan values 
truncated_merged_df['percentage_difference_bt1']=truncated_merged_df['percentage_difference_bt1'].fillna(method='bfill')

#computes the zscore of an array
def compute_z_score(array):
    """
    computes the z_score of a given array
    """
    return stats.zscore(array)

def compute_spread(future,spot):
    "computes the spread given the future and spot price"
    return future-spot

def get_peaks(array,thres,minimum_distance):
    """
    array : ndarray (signed)
        1D amplitude data to search for peaks.
    thres : float between [0., 1.]
        Normalized threshold. Only the peaks with amplitude higher than the
        threshold will be detected.
    minimum_distance : int
        Minimum distance between each detected peak. The peak with the highest
        amplitude is preferred to satisfy this constraint.

    Returns
    -------
    ndarray
        Array containing the numeric indexes of the peaks that were detected
    """
    
    return peakutils.indexes(array,thres=thres,min_dist=minimum_distance)


#compute z score on the spot prices
truncated_merged_df["spot_z_score"]=compute_z_score(truncated_merged_df["price_y"])

#compute spread
truncated_merged_df["spread"]=compute_spread(truncated_merged_df["price_x"],truncated_merged_df["price_y"])


# lets compute the change in spread
truncated_merged_df["spread_change"]=truncated_merged_df.loc[:,"spread_change"]=(truncated_merged_df["spread"].diff(periods=1))

truncated_merged_df["spread_change"]=truncated_merged_df['spread_change'].fillna(method='bfill')

# lets compute the percentage change in spread
truncated_merged_df["percentage_spread_change"]=(truncated_merged_df["spread_change"]/truncated_merged_df["spread"])*100


# Now lets get the spot price indices for the peaks
array=np.array(truncated_merged_df["price_y"])
thres=truncated_merged_df["price_y"].min()/truncated_merged_df["price_y"].max()

minimum_distance=2

#get the maximum peaks indices
indices=get_peaks(array=array,thres=thres,minimum_distance=minimum_distance)

#get the minimum peaks indices
mindices=get_peaks(array=1./array,thres=thres,minimum_distance=minimum_distance)

# set the minimum spot peaks to buy, maximum peaks to sell and no peaks to hold
truncated_merged_df["spot_peaks"]="Hold"
truncated_merged_df.loc[mindices,"spot_peaks"]="Buy"
truncated_merged_df.loc[indices,"spot_peaks"]="Sell"

 
# get percentage difference for future
truncated_merged_df["percentage_difference_future"]=0
truncated_merged_df.loc[:,"percentage_difference_future"]=(truncated_merged_df["price_x"].diff(periods=1)/truncated_merged_df["price_x"])*100


# fill the nan values in percentage_difference_future
truncated_merged_df['percentage_difference_future']=truncated_merged_df['percentage_difference_future'].fillna(method='bfill')

# rename columns to respective names that are easily understandable
truncated_merged_df.rename(columns={'price_x':'future_price','timestamp_x':'timestamp','tickDirection_x':'future_tickDirection','tickDirection_y':'spot_tickDirection', 'price_y':'spot_price', 'symbol_x':'future_symbol', 'symbol_y':'spot_symbol', 'percentage_difference_bt1':'percentage_difference_spot'},inplace=True)


#Now lets get the data we will use for linear regression

lr_df=truncated_merged_df[["timestamp","spot_price","percentage_difference_spot","spot_z_score","spread","percentage_difference_future","future_price"]].copy()


# define Linear regression

reg = LinearRegression(fit_intercept=True)
print(reg)

# split the data into training and testing data with 20 percent going to testing

X_train, X_test, y_train, y_test = train_test_split(lr_df[["spot_price","percentage_difference_spot","spot_z_score","spread"]], lr_df['future_price'], test_size=0.2, random_state=42)

# train the regression model

reg.fit(X_train,y_train)


print("The beta coefficient is {0} and \nThe intercept coefficient is {1}".format(reg.coef_, reg.intercept_))

# Lets predict using our test data
preds=reg.predict(X_test)

# lets calculate the r2 score. closer to 1 is always best
print(r2_score(y_test,preds))

# Spread 
spread_df = pd.DataFrame(lr_df['future_price'] - reg.coef_[0] * lr_df['spot_price'] - reg.intercept_)
spread_df.columns = ['in-sample']
spread_df["Time"]=lr_df['timestamp']
spread_df.set_index("Time")

## Kalman filter
adf = statsmodels.tsa.stattools.adfuller(spread_df['in-sample'], maxlag=1)
print('ADF test statistic: {:.03f}'.format(adf[0]))
print('p-value: {:.03f}'.format(adf[1]))


obs_mat = sm.add_constant(lr_df["spot_price"].values, prepend=False)[:, np.newaxis]

# y is 1-dimensional, (alpha, beta) is 2-dimensional
kf = KalmanFilter(n_dim_obs=1, n_dim_state=2, 
                  initial_state_mean=np.ones(2),
                  initial_state_covariance=np.ones((2, 2)),
                  transition_matrices=np.eye(2),
                  observation_matrices=obs_mat,
                  observation_covariance=1.0,
                  transition_covariance=(1e-5)**2 * np.eye(2))

state_means, state_covs = kf.filter(lr_df["future_price"])

beta_kf = pd.DataFrame({'Slope': state_means[:, 0], 'Intercept': state_means[:, 1]},
                       index=lr_df["timestamp"])

spread_kf = lr_df["future_price"].values - lr_df["spot_price"].values *beta_kf['Slope'].values - beta_kf['Intercept'].values


# Set the rest request client for sending orders. This has to be authenticated
request_client = RequestsClient()
request_client.authenticator = APIKeyAuthenticator(HOST, BITMEX_API_KEY, BITMEX_API_SECRET)

bitMEXAuthenticated = SwaggerClient.from_url(
  SPEC_URI,
  config=config_swag,
  http_client=request_client)

# Save to csv
def write_to_csv(dataframe,spread_zscore,quantity,last_live_zscore):
    """ Saves the trades to csv """
    if isinstance(dataframe, pd.DataFrame):
        dataframe['spread_zscore']=spread_zscore
        dataframe['quantity']=quantity
        dataframe['last_live_zscore']=last_live_zscore
        dataframe=dataframe[['spread_zscore','side','symbol','quantity','ordType','orderID','orderQty','transactTime','price','last_live_zscore']]
        
        dataframe.to_csv('/home/ubuntu/btc/trades_min.csv', mode='a', header=False)

# futures transactions
def buy_futures(symbol="XBTH18",orderQty=1,price=0):
    """
    Buy a specified amount of futures contract at market value
    """
    resp,http_response=bitMEXAuthenticated.Order.Order_new(symbol=symbol,orderQty=orderQty,side='Buy',price=price,
                                                                       ordType="Limit").result()
    
    return resp

def sell_futures(symbol="XBTH18",orderQty=1,price=0):
    """
    Sell a specified amount of futures contract at market value
    
    """
    
    resp,http_response=bitMEXAuthenticated.Order.Order_new(symbol=symbol,orderQty=orderQty,side='Sell',price=price,ordType= "Limit").result()
    
    return resp



# spot transactions

def buy_spot(symbol="XBTUSD",orderQty=1,price=0):
    """
    Buy a specified amount of futures contract at market value
    """
    resp,http_response=bitMEXAuthenticated.Order.Order_new(symbol=symbol,orderQty=orderQty,side='Buy',price=price,ordType= "Limit").result()
    return resp

def sell_spot(symbol="XBTUSD", orderQty=1,price=0):
    """
    Sell a specified amount of futures contract at market value
    
    """
    
    resp,http_response=bitMEXAuthenticated.Order.Order_new(symbol=symbol,orderQty=orderQty,side='Sell',price=price,ordType= "Limit").result()
    return resp


# Check transactions 

def get_positions(symbol):
    """
    get the position given the symbol
    
    """
    resp,http_response=bitMEXAuthenticated.Position.Position_get(filter=json.dumps({"symbol": symbol,"isOpen": True})).result()
    
    # check if there is postion 
    if not resp:
        return None
    
    return resp 



def get_open_orders(symbol):
    
    """
    get the open sell orders
    
    """
    resp,http_response=bitMEXAuthenticated.Order.Order_getOrders(symbol=symbol,reverse=True).result()#filter=json.dumps({"ordStatus" : "New"})
    if not resp:
        return None 
    return resp


# change  order to market 

def change_order_to_market(orderID,price):
    """
    change to market order
    """
    resp,http_response=bitMEXAuthenticated.Order.Order_amend(orderID=orderID,price=price).result()
    
    if not resp:
        return None
    return resp
    
    
# get an instrument details

def get_instrument_details(symbol):
    """
    get the details associated with a certain instrument
    
    returns the a list with instrument details
    
    """
    
    resp,http_response=bitMEX.Instrument.Instrument_get(symbol=symbol,reverse=True).result()
    if not resp:
        return None
    
    return resp

# get nargin leverage used
def margin_leverage():
    res,http=bitMEXAuthenticated.User.User_getMargin().result()
    return res['marginLeverage']
# get active instruments

def get_active_instruments():
    """
    gets the instruments being traded
    
    returns a list of active instruments
        
    """
    resp,http_response=bitMEX.Instrument.Instrument_getActive().result()
    if not resp:
        return None
    
    return resp
   
def check_for_unfilled_orders(orders):
    """
    Check for unfilled orders
    """
    unfilled=[order for order in orders if order["ordStatus"]== "New" or order["ordStatus"]=="PartiallyFilled"]
    
    if not unfilled:
        return None
    
    return unfilled

## getting the ticker data for spot and futures

future = BitMEXWebsocket(endpoint=BITMEX_TEST_WEB_SOCKET_URL, symbol="XBTH18",api_key=BITMEX_API_KEY, api_secret=BITMEX_API_SECRET)

spot = BitMEXWebsocket(endpoint=BITMEX_TEST_WEB_SOCKET_URL, symbol="XBTUSD",
                         api_key=BITMEX_API_KEY, api_secret=BITMEX_API_SECRET)

    #logger.info("Instrument data: %s" % ws.get_instrument())
future.get_instrument()
spot.get_instrument()


# Strategy points ( we reduce the zscore by 40 since its too far from the median and to allow for more trades

# Exit Line (red  dotted on graph)
sell_line= 1#spread_df['in-sample'].mean() + spread_df['in-sample'].std() -40

# Median Line ( blue dotted)

median_line=0

# Buy enter (green dotted line )

buy_line= -1#spread_df['in-sample'].mean() - spread_df['in-sample'].std() +40

#
# x=lr_df["spot_price"].values
# y=lr_df["future_price"].values
# stream the ticker data to determine when the transactions happens
while(future.ws.sock.connected and spot.ws.sock.connected):
    
    # database connection
    conn = MySQLdb.connect(host= "localhost",
                      user="root",
                      passwd="Kapumpum999$",
                      db="btc")
    spot_query="select * from spot;"
    futures_query="select * from future;"
    futures_df = pd.read_sql(futures_query,conn)

    spot_df = pd.read_sql(spot_query,conn)
    
    x=spot_df["last"].values
    y=futures_df["last"].values
    
    # determine if the spread is positive 
    future_price= future.get_ticker()
    spot_price=spot.get_ticker()
    
    # compute the spread
    
    y=np.append(y,future_price["last"])
    x=np.append(x,spot_price["last"])
    y=y[-10000:]
    x=x[-10000:]
    obs_mat = sm.add_constant(x, prepend=False)[:, np.newaxis]

    # y is 1-dimensional, (alpha, beta) is 2-dimensional
    kf = KalmanFilter(n_dim_obs=1, n_dim_state=2, 
                  initial_state_mean=np.ones(2),
                  initial_state_covariance=np.ones((2, 2)),
                  transition_matrices=np.eye(2),
                  observation_matrices=obs_mat,
                  observation_covariance=10**3,
                  transition_covariance=0.01**2 * np.eye(2))
    state_means, state_covs = kf.filter(y)
    beta_kff = pd.DataFrame({'Slope': state_means[:, 0], 'Intercept': state_means[:, 1]})
    spread= y - x*beta_kff['Slope'].values - beta_kff['Intercept'].values
    
    print(spread[-1])
    last_live_zscore=spread[-2]
    spread=spread[-1]
    
    
    spot_symbol="XBTUSD"
    future_symbol="XBTH18"
    
    # quantity to nuy for futures and sell for spot
    sell_buy_quantity=1000
    
    
    
    # check for future open orders
    future_open_orders= get_open_orders(symbol=future_symbol)
    
    # check for spot open orders
    spot_open_orders= get_open_orders(symbol=spot_symbol)
    
    
    
    #check for margin leverage
    margin_leverage_used=margin_leverage()
    
    # check for open orders on one side
    if (future_open_orders is not None):
        
        #change to market
        for future_open_order in future_open_orders:
            if (future_open_order["ordStatus"]== "New" or future_open_order["ordStatus"]=="PartiallyFilled"):
                change_order_to_market(orderID=future_open_order["orderID"],price=future_price["last"])
                sleep(20)
    if (spot_open_orders is not None):
        
        #change to market
        for spot_open_order in spot_open_orders:
            if (spot_open_order["ordStatus"]== "New" or spot_open_order["ordStatus"]=="PartiallyFilled"):
                change_order_to_market(orderID=spot_open_order["orderID"],price=spot_price["last"])
                sleep(20)
    else:
        print("No Open Orders")
        sleep(10)
        
    # check for positions 
     # check for future open orders
    future_open_orders= check_for_unfilled_orders(get_open_orders(symbol=future_symbol))
    
    # check for spot open orders
    spot_open_orders= check_for_unfilled_orders(get_open_orders(symbol=spot_symbol))
    
    future_positions=get_positions(future_symbol)
    
    spot_positions=get_positions(spot_symbol)
    
    if ((spread > sell_line) and  (future_positions is not None and spot_positions is not None) ):#spread%0.5==0
        
       
        
        # check for realised pnl for future
        realised_pnl=[future["realisedPnl"] for future in future_positions]
        
       # check for realised pnl for spot
        realised_spot_pnl=[spot["realisedPnl"] for spot in spot_positions]
        
        
        
        
        
        # check for unrealised pnl in future
        unrealised_pnl=[future["unrealisedPnl"] for future in future_positions]
        
        
        # check for unrealised pnl in spot
        unrealised_spot_pnl=[spot["unrealisedPnl"] for spot in spot_positions]
        
        
        
        # check for quantity of the contracts
        quantity_of_futures=[future["currentQty"] for future in future_positions]
        
        
        # check for the realised profit and see if its greater than unrealised profit
        
        for i,val in enumerate(realised_pnl):
            if( ((val+unrealised_pnl[i]) > 0 or (realised_spot_pnl[i] + unrealised_spot_pnl[i]) >0 ) and( quantity_of_futures[i] >0 and (future_open_orders is None and spot_open_orders is None ))):
                print("Selling")
                print(unrealised_pnl[i])
                
                # if profit from any one leg is greater sell and buy the contracts
                
                sell_response=sell_futures(future_symbol,quantity_of_futures[i],future_price["last"])
                buy_spot_response=buy_spot(spot_symbol,quantity_of_futures[i],spot_price["last"])
                df_buy=pd.DataFrame.from_records([buy_spot_response])
                df_sell=pd.DataFrame.from_records([sell_response])
                write_to_csv(df_sell,spread,quantity_of_futures[i],last_live_zscore)
                write_to_csv(df_buy,spread,quantity_of_futures[i],last_live_zscore)
                
                sleep(20)
                
    
    
    elif ((spread <= buy_line )and future_positions is None): #and spread%0.5==0 
        print("buying")
        buy_response=buy_futures(future_symbol,sell_buy_quantity,float(future_price["last"]))
        sell_spot_response=sell_spot(spot_symbol,sell_buy_quantity,float(spot_price["last"]))
        df_buy=pd.DataFrame.from_records([buy_response])
        df_sell=pd.DataFrame.from_records([sell_spot_response])
        write_to_csv(df_buy,spread,sell_buy_quantity,last_live_zscore)
        write_to_csv(df_sell,spread,sell_buy_quantity,last_live_zscore)
    
    
    elif ((spread <= buy_line  )  and future_positions is not None): #and spread%0.5==0 
        #check if surppassed leverage
        if (future_positions[0]["currentQty"] < 0 and future_positions[0]["unrealisedPnl"] > 0 ):
            print("buying")
            buy_response=buy_futures(future_symbol,sell_buy_quantity,float(future_price["last"]))
            sell_spot_response=sell_spot(spot_symbol,sell_buy_quantity,float(spot_price["last"]))
            df_buy=pd.DataFrame.from_records([buy_response])
            df_sell=pd.DataFrame.from_records([sell_spot_response])
            write_to_csv(df_buy,spread,sell_buy_quantity,last_live_zscore)
            write_to_csv(df_sell,spread,sell_buy_quantity,last_live_zscore)
            
        elif (future_positions[0]["currentQty"] > 0 and margin_leverage_used < 0.40 ):
            print("buying")
            buy_response=buy_futures(future_symbol,sell_buy_quantity,float(future_price["last"]))
            sell_spot_response=sell_spot(spot_symbol,sell_buy_quantity,float(spot_price["last"]))
            df_buy=pd.DataFrame.from_records([buy_response])
            df_sell=pd.DataFrame.from_records([sell_spot_response])
            write_to_csv(df_buy,spread,sell_buy_quantity,last_live_zscore)
            write_to_csv(df_sell,spread,sell_buy_quantity,last_live_zscore)
            
        else:
            print("leverage reached")
                
        
    else:
        if future_positions:
            print(future_positions[0]["leverage"])
        print("No Open futures orders")
                
                
    sleep(50)
          
                
                
        
        
        
        
        
        
        


    
    
        





